﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

	// Helpers
	private bool _isFacingRight = false;

	public bool IsFacingRight
	{
		get { return _isFacingRight; }
		protected set { _isFacingRight = value; }
	}

	public Collider2D EntityCollider { get; protected set; }

	public Color EntityColor = Color.white;

	public bool IgnoreUnityTimeScale { get; set; }

	public bool IsAlive { get; set; }

	// Behaviours
	protected SpriteRenderer spriteRenderer;
	[HideInInspector]
	public MoveBehaviour _moveBehaviour;
	[HideInInspector]
	public HitPointsBehaviour _hpBehaviour;

	/// <summary>
	/// Use this for initialization
	/// </summary>
	public virtual void Initialize()
	{
		EntityCollider = GetComponent<Collider2D>();
		_moveBehaviour = GetComponent<MoveBehaviour>();
		_hpBehaviour = GetComponent<HitPointsBehaviour>();
		IsAlive = true;
		_isFacingRight = true;
		spriteRenderer = GetComponent<SpriteRenderer>();
		if (spriteRenderer)
			EntityColor = spriteRenderer.color;
	}

	void Start () {
		Initialize();
	}

	public virtual void Flip()
	{
		// Switch the way the player is labelled as facing.
		_isFacingRight = !_isFacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
