﻿using UnityEngine;
using System.Collections;

public interface InputInterface {

	Vector2 MoveAxis 
	{
		get;
	}
	bool IsJumping
	{
		get;
	}
	bool IsAttacking
	{
		get;
	}
	bool PausePressed
	{
		get;
	}

	void Update(float delta);
}
