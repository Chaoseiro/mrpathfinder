﻿using UnityEngine;
using System.Collections;

public class InputKeyboard : InputInterface {

	Vector2 _moveAxis;
	bool _isJumping;
	bool _isAttacking;
	bool _pausePressed;

	public Vector2 MoveAxis
	{
		get { return _moveAxis; }
	}

	public bool IsJumping
	{
		get { return _isJumping; }
	}

	public bool IsAttacking
	{
		get { return _isAttacking; }
	}
	public bool PausePressed
	{
		get { return _pausePressed; }
	}

	public void Update(float delta)
	{
		_moveAxis = Vector2.zero;
		
		if (Input.GetKey(KeyCode.A))
			_moveAxis.x = -1;
		if (Input.GetKey(KeyCode.D))
			_moveAxis.x = 1;

        if (Input.GetKey(KeyCode.S))
            _moveAxis.y = -1;

        _pausePressed = Input.GetKeyDown(KeyCode.Escape);

		_isJumping = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W);

		_isAttacking = Input.GetKey(KeyCode.Comma);

	}
}
