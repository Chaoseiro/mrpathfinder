﻿using UnityEngine;
using System.Collections.Generic;
using LikeASir.MrPathfinder;
using System.Collections;
#if X_UNITMX
using X_UniTMX;
#endif

public class LevelLoader : MonoBehaviour {
#if X_UNITMX
    //public TiledMapComponent MapComponent;
    public Material BaseMapMaterial;
	public TextAsset[] _maps;
	public int _currentMap = -1;

	Map _map;
	WaypointMap _waypoints;
	GridMap _gridMap;
	public CameraFolow2D Camera;
	public GameObject PlayerPrefab;
	public GameObject EnemyPrefab;

	float _exitX;
	bool _loadingMap = false;
	Vector2 _playerCachedTransform;
	PlayerController _player;

	Vector3 mapPos;

	public LayerMask UnwalkableMask;
    public LayerMask OneWayPlatformsMask;

    public float GridResolution = 1;

	public bool ShowGridGizmos = false;
	public bool ShowWayPointGizmos = true;

	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 60;
		LoadMap();
	}

	void Update()
	{
		if (_player != null)
		{
			_playerCachedTransform = _player.transform.position;

			if (!_loadingMap && _playerCachedTransform.x >= _exitX)
				LoadMap();

			if (_playerCachedTransform.y < mapPos.y - _map.MapRenderParameter.Height)
				PlayerDied();
		}
	}

	void UnloadMap()
	{
        // Destroy any previous map entities
        var children = new List<GameObject>();
        foreach (Transform child in gameObject.transform)
            children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

	void LoadMap()
	{
		if (_loadingMap)
			return;
		ScreenFade.FadeUp(0.2f, 0, ContinueLoadingMap);
		_loadingMap = true;
	}

	void ContinueLoadingMap()
	{
		UnloadMap();
		_currentMap++;
		if (_currentMap >= _maps.Length)
			_currentMap = 0;
		if (_currentMap < _maps.Length)
		{
            //MapComponent.tmcData.MapTMX = _maps[_currentMap];
            //MapComponent.Initialize(OnMapFinishedLoading);
            _map = new Map(_maps[_currentMap], "Maps/Platformer", OnMapFinishLoading);
		}
	}

    private void OnMapFinishLoading(Map obj)
    {
        obj.Generate(gameObject, BaseMapMaterial, 0, false, false, OnMapFinishedGenerating);
    }

    void ReloadMap()
	{
		if (_loadingMap)
			return;
		_currentMap--;
		LoadMap();
	}

	private void PlayerDied()
	{
		ReloadMap();
	}

	void OnMapFinishedGenerating(Map map)
	{
        _gridMap = null;
        _waypoints = null;

        _map = map;

		mapPos = _map.MapGameObject.transform.position;

		Camera.Bounds = new Rect(mapPos.x, mapPos.y - _map.MapRenderParameter.Height, _map.MapRenderParameter.Width, _map.MapRenderParameter.Height);

        StartCoroutine(FinishGeneratingMap());
    }

    IEnumerator FinishGeneratingMap()
    {
        _map.GenerateTileCollision2DFromLayer("Ground", false, true, "Untagged", LayerMask.NameToLayer("Ground"));
        _map.GenerateColliders2DFromLayer("OneWayPlatforms", true, "Untagged", LayerMask.NameToLayer("OneWayPlatform"));

        yield return null;
        
        CreateGridMap();
        InitializeMapObjects();
    }

	void CreateGridMap()
	{
		int mapWidth = _map.MapRenderParameter.Width;
		int mapHeight = _map.MapRenderParameter.Height;

		_gridMap = new GridMap(mapWidth, mapHeight, mapPos, Vector3.up, GridResolution);

		for (int x = 0; x < _gridMap.Width; x++)
		{
			for (int y = 0; y < _gridMap.Height; y++)
			{
				_gridMap.AddNode(new GridNode(x, y,
                    x + y * mapWidth,
					new Vector3(mapPos.x + (x + 0.5f) * GridResolution, mapPos.y - (y + 0.5f) * GridResolution),
					Physics2D.OverlapArea(new Vector2(mapPos.x + (x + 0.1f) * GridResolution, mapPos.y - (y + 0.1f) * GridResolution), new Vector2(mapPos.x + (x + 0.9f) * GridResolution, mapPos.y - (y + 0.9f) * GridResolution), UnwalkableMask) == null,
					0));
			}
		}
		// Now, for platformer enemies (that does not fly), we convert this gridmap into a waypoint map (map compression)
		_waypoints = GraphMapConverters.GridMapToWaypointMap(_gridMap, 4, 4, UnwalkableMask, OneWayPlatformsMask);
		
	}

	void CreateWaypointMap()
	{
		MapObjectLayer waypointLayer = _map.GetObjectLayer("GroundWaypoints");
		// We use the Object's ID to load the waypoint neighbours
		_waypoints = new WaypointMap(Vector3.zero, Vector3.zero);
		Dictionary<int, PathNode> platformNodes = new Dictionary<int, PathNode>();
		PathNode p = null;
		// First, add all waypoints
		for (int i = 0; i < waypointLayer.Objects.Count; i++)
		{
			MapObject obj = waypointLayer.Objects[i];
			if (obj.Type.Equals("Waypoint"))
			{
				p = new PathNode(platformNodes.Count, obj.Bounds.center, true, 0);
				p.LocalPosition.y *= -1;
				_waypoints.AddNode(p);
				platformNodes.Add(obj.ID, p);
			}
		}
		// Now populate the neighbours
		for (int i = 0; i < waypointLayer.Objects.Count; i++)
		{
			MapObject obj = waypointLayer.Objects[i];
			if (obj.Type.Equals("Waypoint"))
			{
				p = platformNodes[obj.ID];
				// A 'next' makes this edge double-way
				string[] nextWaypoints = obj.GetPropertyAsString("next").Split('|');
				for (int j = 0; j < nextWaypoints.Length; j++)
				{
					int nextID = 0;
					if (int.TryParse(nextWaypoints[j], out nextID))
					{
						p.AddNeighbour(platformNodes[nextID]);
						//if (!platformNodes[nextID].Neighbours.Contains(p))
						//	platformNodes[nextID].AddNeighbour(p);
					}
				}
				// An 'onewaynext' makes this edge one-way
				nextWaypoints = obj.GetPropertyAsString("onewaynext").Split('|');
				for (int j = 0; j < nextWaypoints.Length; j++)
				{
					int nextID = 0;
					if (int.TryParse(nextWaypoints[j], out nextID))
					{
						p.AddNeighbour(platformNodes[nextID], true);
					}
				}
			}
		}
	}

	void InitializeMapObjects()
	{
		MapObjectLayer mol = null;
		Vector3 pos = Vector3.zero;

		mol = _map.GetObjectLayer("Player");
		pos = mol.Objects[0].Bounds.center;
		pos.y *= -1;
		pos += mapPos;
		GameObject player = Instantiate(PlayerPrefab, pos, Quaternion.identity) as GameObject;
		player.transform.parent = _map.MapGameObject.transform;
		Camera.Target = player.transform;
		_player = player.GetComponent<PlayerController>();

		mol = _map.GetObjectLayer("Enemy");
		for (int i = 0; i < mol.Objects.Count; i++)
		{
			pos = mol.Objects[i].Bounds.center;
			pos.y *= -1;
			pos += mapPos;
			GameObject enemy = Instantiate(EnemyPrefab, pos, Quaternion.identity) as GameObject;
			enemy.transform.parent = _map.MapGameObject.transform;
			EnemyAIController eai = enemy.GetComponent<EnemyAIController>();
			eai.AIMap = _waypoints;//_gridMap;//
			eai.Target = player.transform;
		}

		_exitX = mapPos.x + _map.GetObjectLayer("Exit").Objects[0].Bounds.center.x;

		ScreenFade.FadeDown(0.2f, 0);
		_loadingMap = false;
	}

	public void OnDrawGizmos()
	{
		if (_waypoints != null && _waypoints.Nodes != null && ShowWayPointGizmos)
		{
			foreach (var node in _waypoints.Nodes)
			{
				Gizmos.color = Color.black;
				Gizmos.DrawSphere(node.LocalPosition, 0.2f);
				foreach (var n in _waypoints.Neighbours(node))
				{
					Gizmos.color = Color.red;
					Gizmos.DrawLine(node.LocalPosition, n.LocalPosition);
				}
			}	
		}
		if (_gridMap != null && _gridMap.Nodes != null && ShowGridGizmos)
		{
			foreach (var node in _gridMap.Nodes)
			{
				if (node == null)
					continue;
				Gizmos.color = node.IsWalkable ? Color.white : Color.red;
				Gizmos.DrawSphere(node.LocalPosition, 0.2f);
			}
		}
	}
#endif
}
