﻿using LikeASir.MrPathfinder;
#if X_UNITMX
using X_UniTMX;
#endif
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TiledGridMapCreator : GridMapCreator
{
    [Header("Check for Tile in these layers before creating Node")]
    public string[] TileLayerNamesToCheck;

#if X_UNITMX
    TiledMapComponent tmc;

    private void OnEnable()
    {
        if (tmc == null)
            tmc = GetComponent<TiledMapComponent>();

        if(tmc != null)
        {
            GridWidth = tmc.TiledMap.MapRenderParameter.Width;
            GridHeight = tmc.TiledMap.MapRenderParameter.Height;

            AnchorPoint.Set(0, 1, 0);
        }
    }

    protected override void AddNode(int x, int y, Vector3 pos, bool walkable, int cost)
    {   
        if(tmc == null)
            tmc = GetComponent<TiledMapComponent>();
        // before adding the node, check TileLayerNamesToCheck
        if (tmc != null && TileLayerNamesToCheck != null)
        {
            TileLayer layer = null;
            Tile tile = null;
            GridNode node = null;
            for (int i = 0; i < TileLayerNamesToCheck.Length; i++)
            {
                layer = tmc.TiledMap.GetTileLayer(TileLayerNamesToCheck[i]);
                if(layer != null)
                {
                    tile = layer.GetTile(x, y);
                    if(tile != null)
                    {
                        node = new GridNode(x, y, x + y * GridWidth, pos, walkable, cost);
                        break;
                    }
                }
            }
            if(node == null)
            {
                node = new GridNode(x, y, -1, pos, false, cost);
            }
            GridMap.AddNode(node, true, false);
        }
        else
        {
            base.AddNode(x, y, pos, walkable, cost);
        }
    }
#endif
}
