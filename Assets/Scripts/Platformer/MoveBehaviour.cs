﻿using UnityEngine;
using System.Collections;
using System;
#if CHARACTERCONTROLLER
using Prime31;
#endif

//[RequireComponent(typeof(CharacterController2D))]
public class MoveBehaviour : MonoBehaviour
{
    public bool CanMove { set; get; }
	public bool IsMoving { set; get; }

	public bool CanJump { set; get; }
	public bool IsJumping { set; get; }
	public bool IsFalling { set; get; }
	public bool IsWallClinging { set; get; }

    public bool IsDroppingDown { set; get; }

    [HideInInspector]
	public Vector2 MovingDirection, LastMovingDirection;
	[HideInInspector]
	public Vector2 TargetPosition { get; protected set; }

	[HideInInspector]
	public Vector2 MoveVector;

	protected bool InvertFlip;
	protected bool LastFlip;

	public bool UseGravity = true;
	public float Gravity = -9.81f;

	public bool CanWallJump = false;

	//public float MoveAnimationDelay;
	public float MoveSpeed = 5.0f;
	public float CurrentMoveSpeed { get; set; }

	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	float _jumpTimer = 0;
	public float _jumpForgivenessTimer = 0.15f;

	//private float normalizedHorizontalSpeed = 0;
	float smoothedMovementFactor = 0;
	float delta = 0;

#if CHARACTERCONTROLLER
	public CharacterController2D Controller { get; protected set; }
#endif
	protected Entity entity;

	//bool MoveEventSet = false;

	public bool IgnoreUnityTimeScale { get; set; }
	protected float lastTime;

    public LayerMask IgnorePlatformLayer;
    protected float yBeforeDrop = 0;
	protected int StartingLayerMask;

	protected float InitialZ;
	protected Vector3 CachedPosition;

	protected ArrayList delegates = new ArrayList();

	protected EventHandler onFinishedMoving;

	public event EventHandler OnFinishedMoving
	{
		add
		{
			lock (this)
			{
				onFinishedMoving += value;
				delegates.Add(value);
			}
		}
		remove
		{
			lock (this)
			{
				onFinishedMoving -= value;
				delegates.Remove(value);
			}
		}
	}

	public AudioClip[] MoveSFX;

	// Use this for initialization
	void Awake()
	{
		CanMove = true;
		IsMoving = false;

		CanJump = true;
		IsJumping = false;
#if CHARACTERCONTROLLER
		Controller = GetComponent<CharacterController2D>();
#endif
		entity = GetComponent<Entity>();
		StartingLayerMask = gameObject.layer;

		CurrentMoveSpeed = MoveSpeed;

		onFinishedMoving = null;

		//InitialZ = transform.position.z;
		CachedPosition = transform.position;
		InitialZ = CachedPosition.z;

		//if (audio)
		//	audio.volume = Constants.SFXVolume;
	}

	void Update()
	{
#if CHARACTERCONTROLLER
		CachedPosition = transform.position;
		MoveVector = Controller.velocity;

		if (Controller.isGrounded)
		{
			//MoveVector.y = 0;
			IsJumping = false;
			CanJump = true;
			_jumpTimer = 0;
		}
		else
		{
			_jumpTimer += Time.deltaTime;
			if (_jumpTimer >= _jumpForgivenessTimer)
				CanJump = false;
		}

        if (TargetPosition != Vector2.zero)
		{
			IsMoving = true;
			MovingDirection = TargetPosition - (Vector2)CachedPosition;//(Vector2)transform.position;
			MovingDirection.Normalize();
			if (UseGravity)
			{
				MovingDirection.y = 0;
				TargetPosition = new Vector2(TargetPosition.x, CachedPosition.y);//transform.position.y);
			}
			Move(MovingDirection, InvertFlip);
		}

		// apply horizontal speed smoothing it
		smoothedMovementFactor = Controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?

        delta = Time.deltaTime;
		if (IgnoreUnityTimeScale)
			delta = (Time.realtimeSinceStartup - lastTime);

		MoveVector.x = Mathf.Lerp(MoveVector.x, MovingDirection.x * CurrentMoveSpeed, delta * smoothedMovementFactor);

		// apply gravity before moving
		if (UseGravity)
			MoveVector.y += Gravity * delta;

		if (CanWallJump && IsFalling &&
			((Controller.collisionState.left && MovingDirection.x < 0) ||
			(Controller.collisionState.right && MovingDirection.x > 0)))
		{
			IsJumping = false;
			CanJump = true;
			_jumpTimer = 0;
			MoveVector.y /= 1.2f;
			IsWallClinging = true;
		}
		else
			IsWallClinging = false;

		Controller.move(MoveVector * delta);

		lastTime = Time.realtimeSinceStartup;

        if (IsDroppingDown)
        {
            if (CachedPosition.y < yBeforeDrop - 0.1f)
            {
                Controller.platformMask = StartingLayerMask;
                IsDroppingDown = false;
            }
        }

        if (TargetPosition != Vector2.zero)
		{
			/*if (((MovingDirection.x >= 0 && transform.position.x >= TargetPosition.x) ||
				(MovingDirection.x <= 0 && transform.position.x <= TargetPosition.x)) &&
				(MovingDirection.y >= 0 && transform.position.y >= TargetPosition.y) ||
				(MovingDirection.y <= 0 && transform.position.y <= TargetPosition.y))*/
			if (Vector2.Distance(/*transform.position*/ CachedPosition, TargetPosition) < 0.1f)
			{
				//Debug.Log("Reached " + TargetPosition);
				StopMove();
			}
		}
		else
		{
			if (Controller.velocity.sqrMagnitude < 0.01f)
				StopMove();
			else
				IsMoving = true;
		}
		if(MovingDirection != Vector2.zero)
			LastMovingDirection = MovingDirection;
		MovingDirection = Vector2.zero;

		if (Controller.isGrounded)
			IsFalling = false;
		else
		{
			if (Controller.velocity.y < 0)
				IsFalling = true;
		}
#endif
	}

	/// <summary>
	/// Set Entity to move to a designed position.
	/// </summary>
	/// <param name="position">Position that this entity must reach</param>
	/// <param name="invertFlip">True to invert skeleton default flipping (if true, flip will occur when entity moves to the right)</param>
	public virtual void MoveTo(Vector2 position, bool invertFlip = false)
	{
		if (!CanMove)
			return;

		TargetPosition = position;
		InvertFlip = invertFlip;
	}

	/// <summary>
	/// Move in the desired direction
	/// </summary>
	/// <param name="direction">The desired direction to move</param>
	public virtual void Move(Vector2 direction, bool invertFlip = false)
	{
		if (!CanMove)
			return;
		
		InvertFlip = invertFlip;

		direction.Normalize();
		MovingDirection = direction;

		//if ((UseGravity && Controller.isGrounded && ((jumpBehaviour && !jumpBehaviour.IsJumping) || jumpBehaviour == null)) || !UseGravity)
		//{
		//if (jumpBehaviour == null || (jumpBehaviour && !jumpBehaviour.IsJumping))
		//if ((UseGravity && Controller.isGrounded) || !UseGravity)
		//{
		//	MoveVector = direction;
		//	MoveVector *= CurrentMoveSpeed;
		//}
		//else
		//{
		MoveVector.x = direction.x;

		MoveVector.x *= CurrentMoveSpeed;
		//if (runBehaviour && runBehaviour.IsRunning && (jumpBehaviour == null || (jumpBehaviour && jumpBehaviour.IsJumping && jumpBehaviour.StartedJumpRunning)))
		//	MoveVector.x *= runBehaviour.RunSpeed;

		//}

		if (direction.x > 0 && InvertFlip == entity.IsFacingRight)
			entity.Flip();
		if (direction.x < 0 && InvertFlip == !entity.IsFacingRight)
			entity.Flip();
	}

	public virtual void Jump()
	{
		if (!CanJump)
			return;
#if CHARACTERCONTROLLER
		//Move(new Vector2(0, Mathf.Sqrt(2f * jumpHeight * -Gravity)));
		Controller.velocity.y = Mathf.Sqrt(2f * jumpHeight * -Gravity);

		CanJump = false;
		IsJumping = true;
		IsFalling = false;
#endif
	}

	/// <summary>
	/// Stops this entity from moving/end a move
	/// </summary>
	/// <param name="force">True to force the entity from moving, else will check for jump - must maintain momentum when jumping!</param>
	public virtual void StopMove(bool force = false)
	{
		if (force || IsMoving)
		{

			//MoveVector = Vector3.zero;
			MoveVector.x = 0;
			TargetPosition = Vector2.zero;

			IsMoving = false;

			CurrentMoveSpeed = MoveSpeed;

			CanMove = true;
		}
		if (this.onFinishedMoving != null)
			this.onFinishedMoving(this, null);
		//this.onFinishedMoving = null;
	}

    public virtual bool DropDown()
    {
        if (IsDroppingDown)
            return false;
        if (!Controller.isGrounded)
            return false;
        //IsDroppingDown = true;

        //Controller.platformMask = LayerMask.NameToLayer(IgnorePlatformLayer);
        if (Physics2D.Raycast(CachedPosition, -Vector2.up, 0.5f, Controller.oneWayPlatformMask))
        {
            StartingLayerMask = Controller.platformMask;
            yBeforeDrop = CachedPosition.y;
            IsDroppingDown = true;
            Controller.platformMask = IgnorePlatformLayer;
            IsFalling = true;
            Controller.collisionState.reset();
            return true;
        }
        return false;
    }

    public virtual void StopDropDown()
    {
        if (!IsDroppingDown)
            return;
        IsDroppingDown = false;
        Controller.platformMask = StartingLayerMask;
    }

    public virtual void ClearEvents()
	{
		//Debug.Log("Clearing Events");
		//foreach (EventHandler e in delegates)
		for (int i = 0; i < delegates.Count; i++)
		{
			OnFinishedMoving -= (EventHandler)delegates[i];
		}
		onFinishedMoving = null;
		delegates.Clear();
	}

	protected virtual void PlaySFX()
	{
		if (GetComponent<AudioSource>() != null && GetComponent<AudioSource>().enabled)
		{
			GetComponent<AudioSource>().PlayOneShot(MoveSFX[UnityEngine.Random.Range(0, MoveSFX.Length)]);
		}
	}
}
