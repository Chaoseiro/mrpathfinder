﻿using UnityEngine;
using System.Collections;
using LikeASir.MrPathfinder;

public enum EnemyState
{
	// An Idle enemy stays stopped
	IDLE,
	// A Patrolling Enemy moves around its spawn area
	PATROL,
	// A Following Enemy follows the player using a simple follow AI
	FOLLOWING,
	// A Searching enemy is an enemy that lost player's sight and will request a path to the last known player position
	SEARCHING,
	// In this state, the enemy is waiting for the path
	WAITING
	
}

public class EnemyAIController : Entity
{
	public WeightedGraphMap AIMap = null;
	Animator _animator;

	public Transform Target;

	Vector2 _moveAxis = Vector2.zero;
	bool _isJumping = false;
    bool _isDroppingDown = false;

	//Vector3 _lastPoint = Vector3.zero;

	Transform _cachedTransform;
	Vector3 _position;
	Vector3 _startingPosition;
	Vector3 _lastTarget = Vector3.zero;

	int _currentIndex;
	Vector3[] _currentPath;
	Vector3 _currentTarget = Vector3.zero;
	bool _moving = false;
	bool _calculatingPath = false;

	Vector2 diagonalRight = new Vector2(0.75f, -1);
	Vector2 diagonalLeft = new Vector2(-0.75f, -1);

	float _raySizeHorizontal = 0.6f;

	RaycastHit2D hit2D;

	public EnemyState State = EnemyState.IDLE;
	EnemyState _nextState = EnemyState.PATROL;
	EnemyState _previousState = EnemyState.PATROL;
	public float PatrolRadius = 1;
	public float LineOfSight = 4;
	[Range(0, 90)]
	public float LOSAngle = 15;
	public float IdleTimeMin = 0.5f;
	public float IdleTimeMax = 2.5f;
	float _idleTime = 0;
	float _timeSearching = 0;

	bool _targetWasInSight = false;
	public int Stubbornness = 15;
	int _lostSightFrames = 0;

	// Use this for initialization
	void Start()
	{
		Initialize();

		_cachedTransform = transform;
#if CHARACTERCONTROLLER
		_raySizeHorizontal = _moveBehaviour.Controller.boxCollider.size.x / 2.0f + 0.05f;
#endif
		_currentTarget = _startingPosition = _cachedTransform.position;

		_animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
	{
		if (!IsAlive)
			return;

		//CheckJump();
		_moveBehaviour.Move(_moveAxis);

		_animator.SetBool("IsWalking", _moveBehaviour.IsMoving);

		if (_isJumping)
		{
			_moveBehaviour.Jump();
			if (_moveBehaviour.IsJumping)
			{
				_animator.SetBool("Jump", true);
			}
		}
        if(_isDroppingDown)
        {
            _moveBehaviour.DropDown();    
            _isDroppingDown = false;
        }
		if (_moveBehaviour.IsFalling)
			_animator.SetBool("Jump", false);

		_animator.SetBool("Fall", _moveBehaviour.IsFalling);
	}

	bool CanSeeTarget()
	{
		if (Target == null)
			return false;

		_position = (Vector2)_cachedTransform.position + 0.5f * Vector2.up;
		// check direction between target and enemy. if it is inside LOSAngle, raycast.
		Vector2 direction = Target.position + 0.5f * Vector3.up - _position;

		// too far away?
		if (direction.sqrMagnitude > LineOfSight * LineOfSight)
			return false;

		direction.Normalize();
		if ((IsFacingRight && direction.x < 0) || (!IsFacingRight && direction.x > 0))
			return false;
		float angle = 0;
		// If the enemy saw the player, we do not take the LOS angle into account anymore
		if (!_targetWasInSight)
		{
			angle = Vector2.Angle(direction, Vector2.right);
			if (angle > 90)
				angle = 180 - angle;
		}
		if (_targetWasInSight || angle <= LOSAngle)
		{
#if CHARACTERCONTROLLER
			hit2D = Physics2D.Raycast(_position, direction/*IsFacingRight ? Vector2.right : -Vector2.right*/, LineOfSight, (1 << Target.gameObject.layer) | _moveBehaviour.Controller.platformMask);
#else
            hit2D = Physics2D.Raycast(_position, direction, LineOfSight, (1 << Target.gameObject.layer));
#endif
            Debug.DrawRay(_position, direction * LineOfSight, Color.cyan);
			if (hit2D.collider != null)
			{
				if (hit2D.collider.gameObject.layer.Equals(Target.gameObject.layer))
					return true;
			}
		}
		return false;
	}

	void FixedUpdate()
	{
		_moveAxis = Vector2.zero;
		_position = (Vector2)_cachedTransform.position + 0.5f * Vector2.up;
		switch (State)
		{
			case EnemyState.IDLE:
				if (CanSeeTarget())
				{
					SetState(EnemyState.FOLLOWING);
					break;
				}
				// stay still for some time then go back to patrol				
				if (_idleTime > 0)
					_idleTime -= Time.fixedDeltaTime;
				
				if(_idleTime <= 0)
					SetState(_nextState);
				if (CanSeeTarget())
					SetState(EnemyState.FOLLOWING);
				break;
			case EnemyState.PATROL:
				// move inside patrol radius
				if (_moving)
				{
					if (Mathf.Abs(_currentTarget.x - _position.x) < 0.1f)
					{
						SetState(EnemyState.IDLE, EnemyState.PATROL);
						_moving = false;
					}
					
				}
				else
				{
					_currentTarget = _startingPosition + Random.Range(-PatrolRadius, PatrolRadius) * Vector3.right;
					
					// we are under or above patrolling point, so pathfind the way to it
					if (Mathf.Abs(_currentTarget.y - _position.y) > 2f)
					{
						_currentPath = null;
						CalculatePath(_startingPosition);
						SetState(EnemyState.WAITING, EnemyState.SEARCHING);
						break;
					}
					_moving = true;
				}

				_moveAxis.x = _currentTarget.x - _position.x;

				if (CanSeeTarget())
					SetState(EnemyState.FOLLOWING);
				break;
			case EnemyState.FOLLOWING:
				if(Target == null)
				{
					SetState(EnemyState.IDLE, EnemyState.PATROL);
					break;
				}

				_currentTarget = Target.position;

				if (CanSeeTarget())
				{
					_targetWasInSight = true;
					_lostSightFrames = 0;
				}
				else
				{
                    if (!_isJumping && _position.y - 0.5f > _currentTarget.y)
                    {
                        // try dropping down
                        _isDroppingDown = true;
                    }
                    // First wait for some frames before assuming the player has completely left the LOS
                    _lostSightFrames++;
					if (_targetWasInSight && _lostSightFrames > Stubbornness)
					{
						_lostSightFrames = 0;
						_targetWasInSight = false;
						CalculatePath(Target.position);
						SetState(EnemyState.IDLE, EnemyState.SEARCHING);
						break;
					}
				}
				// follow the player by simply moving to its position
				_moveAxis.x = _currentTarget.x - _position.x;
				break;
			case EnemyState.SEARCHING:
				if (Target == null)
				{
					SetState(EnemyState.IDLE, EnemyState.PATROL);
					break;
				}
				if (CanSeeTarget())
				{
					SetState(EnemyState.FOLLOWING);
					break;
				}

				if (_calculatingPath)
				{
					Debug.Log("Calculating...");
					break;
				}
				
				if (_currentPath == null || _currentIndex < 0 || _currentIndex > _currentPath.Length - 1)
				{
					_moving = false;
					_moveAxis = Vector2.zero;
					_isJumping = false;
					_currentTarget = _startingPosition;
					//_currentIndex = 0;
					if (_lastTarget.Equals(_startingPosition))
					{
						SetState(EnemyState.IDLE, EnemyState.PATROL);
					}
					else
					{
						_currentPath = null;
						CalculatePath(_startingPosition);
						SetState(EnemyState.IDLE, EnemyState.SEARCHING);
					}
					break;
				}

				_currentTarget = _currentPath[_currentIndex];

				if ((_currentTarget - _position).sqrMagnitude < 0.5f)
				{
					_currentIndex++;
					//if (_currentIndex >= _currentPath.Length - 1)
					//	_currentTarget = Target.position;
				}

				if (Mathf.Abs(_currentTarget.y - _position.y) > 2.0f)
				{
                    if(_position.y > _currentTarget.y)
                    {
                        // try dropping down
                        _isDroppingDown = true;
                    }
					_timeSearching += Time.fixedDeltaTime;
					if (_timeSearching > 5)
					{
						// something might have gone wrong... recalculate path
						CalculatePath(_currentPath[_currentPath.Length - 1]);
						SetState(EnemyState.IDLE, EnemyState.SEARCHING);
						_currentPath = null;
						break;
					}
				}

				_moveAxis.x = _currentTarget.x - _position.x;

				for (int i = 0; i < _currentPath.Length - 1; i++)
				{
					UnityEngine.Debug.DrawLine(_currentPath[i], _currentPath[i + 1], Color.cyan);
				}
				break;
			case EnemyState.WAITING:
				_moveAxis = Vector2.zero;
				if (_currentPath != null)
					SetState(EnemyState.IDLE);
				break;
		}
		CheckJump();
	}

	void CheckJump()
	{
		if (State.Equals(EnemyState.IDLE) || State.Equals(EnemyState.WAITING))
			return;

		Debug.DrawRay(_position, (_moveAxis.x > 0 ? diagonalRight : diagonalLeft) * 0.7f, Color.magenta);
		_isJumping = false;
    #if CHARACTERCONTROLLER
		// we won't jump when patrolling, just stop at the edge
		hit2D = Physics2D.Raycast(_position, _moveAxis.x > 0 ? Vector2.right : -Vector2.right, _raySizeHorizontal, _moveBehaviour.Controller.platformMask);
		if (hit2D.collider != null)
		{
			if (!State.Equals(EnemyState.PATROL))
				_isJumping = true;
			else if(_previousState.Equals(EnemyState.PATROL))
			{
				_currentTarget = _startingPosition;
				//_moveAxis = Vector2.zero;
				_moveBehaviour.StopMove(true);
				SetState(EnemyState.IDLE, EnemyState.PATROL);
			}
		}
		else
		{
			hit2D = Physics2D.Raycast(_position, _moveAxis.x > 0 ? diagonalRight : diagonalLeft, 0.7f, _moveBehaviour.Controller.platformMask);
			if (hit2D.collider == null)
			{
				if (State.Equals(EnemyState.PATROL) && _previousState.Equals(EnemyState.PATROL))
				{
					_currentTarget = _startingPosition;
					//_moveAxis = Vector2.zero;
					_moveBehaviour.StopMove(true);
					SetState(EnemyState.IDLE, EnemyState.PATROL);
				} 
				else if (_currentTarget.y - _position.y > 0.05f)
				{
					_isJumping = true;
				}
			}
		}
#endif
	}

	public void SetState(EnemyState newState)
	{
		_previousState = State;
		if (newState.Equals(EnemyState.IDLE))
			_idleTime = Random.Range(IdleTimeMin, IdleTimeMax);

		State = newState;
	}

	public void SetState(EnemyState newState, EnemyState nextState)
	{
		_previousState = State;
		_nextState = nextState;
		if(newState.Equals(EnemyState.IDLE))
			_idleTime = Random.Range(IdleTimeMin, IdleTimeMax);

		State = newState;
	}

	void CalculatePath(Vector3 target)
	{
		if (_moving || AIMap == null || _calculatingPath)
			return;
		_lostSightFrames = 0;
		_calculatingPath = true;
		_lastTarget = target;
		_timeSearching = 0;
		PathFindManager.Instance.RequestPath(_position, target, AIMap, OnPathCalculated);
	}

	void OnPathCalculated(Vector3[] path, bool success)
	{
		_calculatingPath = false;
		if (success)
		{
			_currentPath = path; //as PathNode[];
			_currentIndex = 0;
			_currentTarget = _currentPath[0];
			_moving = true;
		}
		else
		{
			// something went really wrong here...
			Debug.LogWarning("Path not found!");
			SetState(EnemyState.PATROL);
		}
	}
}
