﻿using UnityEngine;
using System.Collections;

public class PlayerController : Entity {

	public InputInterface _inputController;

	Animator _animator;

	// Use this for initialization
	void Start () {
		Initialize();

		_inputController = new InputKeyboard();

		_animator = GetComponent<Animator>();

		//_hpBehaviour.OnHit += onHit;
	}

	private void onHit(Entity arg1, int arg2)
	{
		_animator.SetTrigger("Hit");
	}
	
	// Update is called once per frame
	void Update () {
		if (!IsAlive)
			return;
		//_characterController.move((_inputController.MoveAxis + Physics2D.gravity) * _moveSpeed);
		if (_inputController == null)
			return;
		_inputController.Update(Time.deltaTime);
		
		_moveBehaviour.Move(_inputController.MoveAxis);

		_animator.SetBool("IsWalking", _moveBehaviour.IsMoving);

		if (_inputController.IsJumping)
		{
            if (_inputController.MoveAxis.y < 0)
                _moveBehaviour.DropDown();
            else
			    _moveBehaviour.Jump();
			if (_moveBehaviour.IsJumping)
			{
				_animator.SetBool("Jump", true);
			}
		}
		if (_moveBehaviour.IsFalling)
			_animator.SetBool("Jump", false);

		_animator.SetBool("Fall", _moveBehaviour.IsFalling);	
	}
}
