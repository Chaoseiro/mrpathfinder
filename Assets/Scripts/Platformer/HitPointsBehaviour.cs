﻿using UnityEngine;
#if CHARACTERCONTROLLER
using Prime31;
#endif

public class HitPointsBehaviour : MonoBehaviour {

	// Hit Points
	protected int _hitPoints;

	public int HitPoints
	{
		get { return _hitPoints; }
		protected set { _hitPoints = value; }
	}
	public int StartingHitPoints;

	// Stun/invicible
	public float InvincibleTimeAfterHit = 0;
	protected float invincibleTimer = 0;
	public bool IsInvincible { get; set; }

	float stunTimer;
	public bool IsStunned { get; protected set; }

	Vector2 Knockback = Vector2.zero;

    // Behaviours
#if CHARACTERCONTROLLER
    CharacterController2D _controller;
#endif
	Entity _baseEntity;

	// Events
	public System.Action<Entity> OnDie = delegate { };
	public System.Action<Entity, int> OnHit = delegate { };

	// SFX
	public AudioClip[] DeathSFX;

	protected void Initialize()
	{
		_baseEntity = GetComponent<Entity>();
		_hitPoints = StartingHitPoints;
#if CHARACTERCONTROLLER
		_controller = GetComponent<CharacterController2D>();
#endif
	}

	void Start()
	{
		Initialize();
	}

	void FixedUpdate()
	{
		InternalUpdate();
	}

	protected virtual void InternalUpdate()
	{
#if CHARACTERCONTROLLER
		if (_controller != null && Knockback.magnitude > 0.1f)
			_controller.move(Knockback * Time.fixedDeltaTime);
#endif
		Knockback = Vector2.Lerp(Knockback, Vector2.zero, 5 * Time.fixedDeltaTime);
		if (!_baseEntity.IsAlive)
			return;
		if (stunTimer > 0)
		{
			stunTimer -= Time.fixedDeltaTime;

			if (stunTimer <= 0)
			{
				IsStunned = false;
			}
		}
		if (invincibleTimer > 0)
		{
			invincibleTimer -= Time.fixedDeltaTime;
			if (invincibleTimer <= 0)
			{
				IsInvincible = false;
			}

		}
	}

	public virtual void Hit(int damage, Vector2 knockDistance)
	{
		if (!_baseEntity.IsAlive || IsInvincible)
			return;
		
		invincibleTimer = InvincibleTimeAfterHit;
		if (invincibleTimer > 0)
			IsInvincible = true;

		
		HitPoints -= damage;
#if CHARACTERCONTROLLER
		if (_controller)
		{
			Knockback += knockDistance;
		}
#endif
		if (OnHit != null)
			OnHit(_baseEntity, damage);

		if (HitPoints < 0)
			HitPoints = 0;

		if (HitPoints < 1)
		{
			Die();
		}
	}

	public virtual void Die()
	{
		_baseEntity.IsAlive = false;

		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		//gameObject.layer = LayerMask.NameToLayer("IgnorePlayer");
		_baseEntity.EntityCollider.enabled = false;
		if (_baseEntity.EntityCollider.GetComponent<Rigidbody2D>())
		{
			_baseEntity.EntityCollider.GetComponent<Rigidbody2D>().angularVelocity = 0;
			_baseEntity.EntityCollider.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}

		if (OnDie != null)
			OnDie(_baseEntity);

		PlayDeathSFX();
	}

	protected virtual void PlayDeathSFX()
	{
		if (GetComponent<AudioSource>() != null && DeathSFX.Length > 0)
		{
			GetComponent<AudioSource>().PlayOneShot(DeathSFX[UnityEngine.Random.Range(0, DeathSFX.Length)]);
		}
	}
}
