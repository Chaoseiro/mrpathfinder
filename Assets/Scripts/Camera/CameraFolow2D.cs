using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CameraFolow2D : Entity
{
    public float smoothMovement = 3.0f;        
	public Transform Target = null;
	public Vector3 Offset = Vector3.zero;
	public Rect Bounds = new Rect();
    Camera m_camera;
	Vector3 cachedPosition;

	bool isShaking = false;
	float shakeSeconds = 0;
	float shakeStrength = 0.1f;
	float lastTime;

	Vector3 GetPlayerPosition ()
	{
		return new Vector3 (Target.position.x + Offset.x, // get X from player
							Target.position.y + Offset.y, // get Yfrom player
							transform.position.z + Offset.z);  // get X from camera
	}

	void TryToFindPlayer ()
	{
		// Obtem a instancia do objeto do jogador
		//TargetTag = FindObjectOfType<Character2DController> ();
		if(Target != null) {
			// Set current position camera to player
			transform.position = GetPlayerPosition ();
		}
	}

    private void Awake()
    {
        m_camera = GetComponent<Camera>();
    }

    void Start ()
    {
        TryToFindPlayer ();
    }

    void Update ()
    {
		if(Target == null) {
			TryToFindPlayer ();
			if(Target == null) return;
		}

		cachedPosition = transform.position;

		Vector3 newPosition =  GetPlayerPosition ();
        
        // Interpolate camera position to player
		cachedPosition = Vector3.Lerp(cachedPosition, newPosition, smoothMovement * Time.deltaTime);

		if (cachedPosition.x - m_camera.orthographicSize * m_camera.aspect < Bounds.xMin)
			cachedPosition.x = Bounds.xMin + m_camera.orthographicSize * m_camera.aspect;
		if (cachedPosition.x + m_camera.orthographicSize * m_camera.aspect > Bounds.xMax)
			cachedPosition.x = Bounds.xMax - m_camera.orthographicSize * m_camera.aspect;
		if (cachedPosition.y - m_camera.orthographicSize < Bounds.yMin)
			cachedPosition.y = Bounds.yMin + m_camera.orthographicSize;
		if (cachedPosition.y + m_camera.orthographicSize > Bounds.yMax)
			cachedPosition.y = Bounds.yMax - m_camera.orthographicSize;

		if (isShaking)
		{
			shakeSeconds -= IgnoreUnityTimeScale ? (Time.realtimeSinceStartup - lastTime) : Time.deltaTime;
			lastTime = Time.realtimeSinceStartup;
			if (shakeSeconds <= 0)
				isShaking = false;

			cachedPosition += new Vector3(Random.Range(-shakeStrength, shakeStrength), Random.Range(-shakeStrength, shakeStrength));
		}

		transform.position = cachedPosition;
		
    }

	public void ShakeCamera(float seconds, float ShakeStrength = 0.1f)
	{
		isShaking = true;
		shakeSeconds = seconds;
		shakeStrength = ShakeStrength;
		lastTime = Time.realtimeSinceStartup;
	}
}