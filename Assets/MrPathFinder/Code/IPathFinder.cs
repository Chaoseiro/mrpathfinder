﻿using UnityEngine;
using System.Collections;

namespace LikeASir.MrPathfinder
{
	public interface IPathFinder<L, T>
		where L : Node
		where T : WeightedGraphMap
	{
		void StartFindPath(Vector3 from, Vector3 to, T graph);
		IEnumerator GetPath(Vector3 from, Vector3 to, T graph);
		Vector3[] RetracePath(L start, L goal);
	}
}
