﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
	public interface IPathRequest<T>
		//where L : Node
		where T : WeightedGraphMap
	{
		Vector3 PathStart { get; }
		Vector3 PathEnd { get; }
		T Graph { get; }
		System.Action<Vector3[], bool> Callback { get; }
	}
}