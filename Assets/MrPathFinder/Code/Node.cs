﻿using UnityEngine;
using System;

namespace LikeASir.MrPathfinder
{
    [System.Serializable]
    public class Node : IEquatable<Node>
	{
        public int NodeIndex = int.MinValue;

		public int Cost = int.MaxValue;
		public int hCost = 1;
		public int gCost = 1;
		public int TotalCost { get { return hCost + gCost; } }
		public bool IsWalkable = true;
		// for better results, consider this position as a local position in relation to the graph's world position
		public Vector3 LocalPosition = Vector3.zero;

		public int NodeParentIndex = -1;
        [SerializeField]
		protected int heapIndex;

		public int HeapIndex
		{
			get
			{
				return heapIndex;
			}
			set
			{
				heapIndex = value;
			}
		}

		public Node(int nodeIndex, Vector3 localPosition, bool walkable, int cost)
		{
            NodeIndex = nodeIndex;
			LocalPosition = localPosition;
			IsWalkable = walkable;
			heapIndex = 0;
			Cost = cost;
		}

		public virtual int CompareTo(Node other)
		{
			int compare = TotalCost.CompareTo(other.TotalCost);
			if (compare == 0)
				compare = hCost.CompareTo(other.hCost);

			return -compare;
		}

        public bool Equals(Node other)
        {
            return NodeIndex == other.NodeIndex && Cost == other.Cost;
        }
    }
}