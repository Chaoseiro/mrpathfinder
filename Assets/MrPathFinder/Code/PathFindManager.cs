﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

namespace LikeASir.MrPathfinder
{
	public struct PathRequest : IPathRequest<WeightedGraphMap>
	{
		public Vector3 pathStart;
		public Vector3 pathEnd;
		public WeightedGraphMap graph;
		public System.Action<Vector3[], bool> callback;

		public Vector3 PathStart { get { return pathStart; } }
		public Vector3 PathEnd { get { return pathEnd; } }
		public WeightedGraphMap Graph { get { return graph; } }
		public System.Action<Vector3[], bool> Callback { get { return callback; } }

		public PathRequest(Vector3 _start, Vector3 _end, WeightedGraphMap _graph, System.Action<Vector3[], bool> _callback)
		{
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
			graph = _graph;
		}
	}

	public class PathFindManager : MonoBehaviour, IPathFindManager<WeightedGraphMap>
	{
		static PathFindManager _instance;

		static bool _beingDestroyed = false;

		public static PathFindManager Instance
		{
			get
			{
				if (_beingDestroyed)
				{
					Debug.LogWarning("PathFindManager is being destroyed but some script is trying to access it. Returning null.");
					return null;
				}

				_instance = FindObjectOfType<PathFindManager>();

				if (FindObjectsOfType<PathFindManager>().Length > 1)
				{
					Debug.LogError("[Singleton] Something went really wrong " +
								" - there should never be more than 1 singleton!" +
								" Reopening the scene might fix it.");
				}

				if (_instance == null)
				{
					GameObject go = new GameObject("[Singleton] PathFindManager");
					_instance = go.AddComponent<PathFindManager>();
					DontDestroyOnLoad(_instance);

                    SceneManager.sceneLoaded += OnSceneLoaded;
				}

				return _instance;
			}
		}

        private static void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            // clear the queue when level is loaded
            pathRequestQueue.Clear();
        }

        void OnDestroy()
		{
			_beingDestroyed = true;
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

		static Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
		static PathRequest _currentPathRequest;
		static bool _isProcessing = false;

		public void RequestPath(Vector3 pathStart, Vector3 pathEnd, WeightedGraphMap graph, System.Action<Vector3[], bool> callback)
		{
			PathRequest newRequest = new PathRequest(pathStart, pathEnd, graph, callback);
			pathRequestQueue.Enqueue(newRequest);
			TryProcessNext();
		}

		void TryProcessNext()
		{
			if (!_isProcessing && pathRequestQueue.Count > 0)
			{
				_currentPathRequest = pathRequestQueue.Dequeue();
				_isProcessing = true;
				Pathfinder.Instance.StartFindPath(_currentPathRequest.pathStart, _currentPathRequest.pathEnd, _currentPathRequest.graph);
			}
		}

		public void FinishedProcessingPath(Vector3[] path, bool success)
		{
			_currentPathRequest.callback(path, success);
			_isProcessing = false;
			TryProcessNext();
		}
        /*
		public void OnLevelWasLoaded(int level)
		{
			// clear the queue when level is loaded
			pathRequestQueue.Clear();
		}
        */

	}

	public class Pathfinder : MonoBehaviour, IPathFinder<Node, WeightedGraphMap>
	{

		static Pathfinder _instance;

		static bool _beingDestroyed = false;

		public static Pathfinder Instance
		{
			get
			{
				if (_beingDestroyed)
				{
					Debug.LogWarning("Pathfinder is being destroyed but some script is trying to access it. Returning null.");
					return null;
				}

				_instance = FindObjectOfType<Pathfinder>();

				if (FindObjectsOfType<Pathfinder>().Length > 1)
				{
					Debug.LogError("[Singleton] Something went really wrong " +
								" - there should never be more than 1 singleton!" +
								" Reopening the scene might fix it.");
				}

				if (_instance == null)
				{
					GameObject go = new GameObject("[Singleton] Pathfinder");
					_instance = go.AddComponent<Pathfinder>();
					DontDestroyOnLoad(_instance);
				}

				return _instance;
			}
		}

		void OnDestroy()
		{
			_beingDestroyed = true;
		}

		WeightedGraphMap _graph = null;

		public void StartFindPath(Vector3 from, Vector3 to, WeightedGraphMap graph)
		{
			_graph = graph;
			StartCoroutine(GetPath(from, to, graph));
		}

		public IEnumerator GetPath(Vector3 from, Vector3 to, WeightedGraphMap graph)
		{
			Vector3[] waypoints = null;
			bool pathSucess = false;

			if (graph != null)
			{
				// first, find the closest nodes in the list of waypoints
				Node begin = graph.WorldPositionToNode(from);

				Node goal = graph.WorldPositionToNode(to);

				// If the goal is at the beginning, nothing to do here
				if (!begin.LocalPosition.Equals(goal.LocalPosition) && begin.IsWalkable && goal.IsWalkable)
				{
					//Dictionary<Node, int> costSoFar = new Dictionary<Node, int>();

					HashSet<Node> path = new HashSet<Node>();
					Heap<Node> frontier = new Heap<Node>(graph.NodeCount);
					frontier.Add(begin);
					Node current = null;

					//costSoFar.Add(begin, 0);

					while (frontier.Count > 0)
					{
						current = frontier.RemoveFirst();
						path.Add(current);

						if (current.Equals(goal))
						{
							pathSucess = true;
							break;
						}
						IEnumerable<Node> neighbours = graph.Neighbours(current);
						foreach (var next in neighbours)
						{
							if (!next.IsWalkable || path.Contains(next))
								continue;

							int neighbourCost = current.gCost + graph.Heuristic(current, next) + graph.Cost(current, next);
							//int neighbourCost = costSoFar[current] + graph.Cost(current, next);

							if (neighbourCost < next.gCost || !frontier.Contains(next)) 
							//if (!costSoFar.ContainsKey(next) || neighbourCost < costSoFar[next])
							{
								next.gCost = neighbourCost;
								next.hCost = graph.Heuristic(next, goal);
								next.NodeParentIndex = current.NodeIndex;
								//if (!costSoFar.ContainsKey(next))
								//	costSoFar.Add(next, neighbourCost);
								if (!frontier.Contains(next))
									frontier.Add(next);
								else
									frontier.UpdateItem(next);
							}
						}
					}
				}
				yield return null;
				if (pathSucess)
				{
					waypoints = RetracePath(begin, goal);
				}
			}
			PathFindManager.Instance.FinishedProcessingPath(waypoints, pathSucess);
		}

		public Vector3[] RetracePath(Node start, Node goal)
		{
			if (start == null || goal == null)
				return null;

			List<Vector3> p = new List<Vector3>();

			Node current = goal;
			p.Add(_graph.NodePositionToWorldPosition(current));
			while (current != start)
			{
				p.Add(_graph.NodePositionToWorldPosition(_graph.GetNodeFromIndex(current.NodeParentIndex)));
				current = _graph.GetNodeFromIndex(current.NodeParentIndex);
			}
			p.Reverse();
			return p.ToArray();//SimplifyPath(p);//
		}
		// TODO: Fix this!
		Node[] SimplifyPath(List<Node> path)
		{
			List<Node> waypoints = new List<Node>();
			Vector2 directionOld = Vector2.zero;

			for (int i = 1; i < path.Count; i++)
			{
				Vector2 directionNew = new Vector2(path[i - 1].LocalPosition.x - path[i].LocalPosition.x, path[i - 1].LocalPosition.y - path[i].LocalPosition.y);
				if (directionNew != directionOld)
				{
					waypoints.Add(path[i]);
				}
				directionOld = directionNew;
			}
			return waypoints.ToArray();
		}
	}
}