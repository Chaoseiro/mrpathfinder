﻿using UnityEngine;
using System.Collections;

namespace LikeASir.MrPathfinder
{
    [System.Serializable]
	public class GridNode : Node
	{
		public int x;
		public int y;

		public GridNode(int gridX, int gridY, int nodeIndex, Vector3 localPosition, bool walkable, int cost)
			: base(nodeIndex, localPosition, walkable, cost)
		{
			x = gridX;
			y = gridY;
		}
	}
}
