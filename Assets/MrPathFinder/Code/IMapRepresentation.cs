﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
	public interface IMapRepresentation<L> where L : Node
	{
		IEnumerable<L> Nodes { get; }
		int NodeCount { get; }
		int Cost(L a, L b);
		int Heuristic(L a, L b);
		IEnumerable<L> Neighbours(L node);
		L WorldPositionToNode(Vector3 worldPosition);
		void AddNode(Vector3 worldPosition, bool walkable);
		void AddNode(L node);
		void SetNodes(IEnumerable<L> nodes);
	}
}
