﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
    [System.Serializable]
    public class WaypointMap : WeightedGraphMap
	{
        // Dictionary of distances of NodeIndex to dictionary of NodeIndex and Distances
		Dictionary<int, Dictionary<int, int>> _distances;

        [SerializeField]
		List<PathNode> _nodes;
        [SerializeField]
        List<Vector3> _nodesPosition;

		public List<Vector3> Waypoints { get { return _nodesPosition; } }

		public override Node[] Nodes
		{
			get
			{
                if (_nodes == null)
                    return null;
				return _nodes.ToArray();
			}
		}

		public override int NodeCount { get { return _nodes == null ? 0 : _nodes.Count; } }

        public WaypointMap(Vector3 position, Vector2 anchorPoint, GraphAxis axis = GraphAxis.XandY)
		{
			_anchorPoint = anchorPoint;
			_worldPosition = position;
			_axis = axis;
		}

		public override void AddNode(Vector3 worldPosition, bool walkable, int cost)
		{
			if (_nodes == null)
			{
				_nodes = new List<PathNode>();
				_nodesPosition = new List<Vector3>();
			}
			Vector3 nodeLocalPos = worldPosition - _worldPosition;
			_nodes.Add(new PathNode(_nodes.Count, nodeLocalPos, walkable, cost));
			_nodesPosition.Add(nodeLocalPos);
		}

		public override void AddNode(Node node)
		{
			if (_nodes == null)
			{
				_nodes = new List<PathNode>();
				_nodesPosition = new List<Vector3>();
			}
			// assume node.position is in world coordinates
			node.LocalPosition -= _worldPosition;
            node.NodeIndex = _nodes.Count;
			_nodes.Add(node as PathNode);
			_nodesPosition.Add(node.LocalPosition);
		}

		public void SetNodes(IEnumerable<PathNode> nodes)
		{
			if (_nodes == null)
			{
				_nodes = new List<PathNode>();
				_nodesPosition = new List<Vector3>();
			}
			else
			{
				_nodes.Clear();
				_nodesPosition.Clear();
			}
			foreach (var node in nodes)
			{
				// assume node.position is in world coordinates
				node.LocalPosition -= _worldPosition;
                node.NodeIndex = _nodes.Count;
				_nodes.Add(node);
				_nodesPosition.Add(node.LocalPosition);
			}
		}

		public override void RemoveNode(Node node)
		{
			PathNode p = node as PathNode;
			foreach (var n in _nodes)
			{
				if (!n.Equals(node))
				{
					n.RemoveNeighbour(p);
				}
			}
			int index = _nodes.IndexOf(p);
			_nodes.RemoveAt(index);
			_nodesPosition.RemoveAt(index);
            // Fix node indexes
            for (int i = index; i < _nodes.Count; i++)
            {
                _nodes[index].NodeIndex = i;
            }
		}

		public override int Cost(Node a, Node b)
		{
			return Heuristic(a, b);
		}

		public override int Heuristic(Node a, Node b)
		{
			PathNode pa = a as PathNode;
			PathNode pb = b as PathNode;
			Dictionary<int, int> distanceFromA;
			if (_distances == null)
				_distances = new Dictionary<int, Dictionary<int, int>>();
			if (_distances.TryGetValue(pa.NodeIndex, out distanceFromA))
			{
				if (distanceFromA.ContainsKey(pb.NodeIndex))
				{
					// check if any position has updated
					bool updateDistance = false;
					//int indexOfA = _nodes.IndexOf(pa);
					//int indexOfB = _nodes.IndexOf(pb);
					if (a.LocalPosition != _nodesPosition[pa.NodeIndex])
					{
						_nodesPosition[pa.NodeIndex] = a.LocalPosition;
						updateDistance = true;
					}
					if (b.LocalPosition != _nodesPosition[pb.NodeIndex])
					{
						_nodesPosition[pb.NodeIndex] = b.LocalPosition;
						updateDistance = true;
					}
					if (updateDistance)
						distanceFromA[pb.NodeIndex] = Mathf.RoundToInt((a.LocalPosition - b.LocalPosition).sqrMagnitude);
					return distanceFromA[pb.NodeIndex];
				}
				else
				{
					int d = Mathf.RoundToInt((a.LocalPosition - b.LocalPosition).sqrMagnitude);
					distanceFromA.Add(pb.NodeIndex, d);
					return d;
				}
			}
			else
			{
				Dictionary<int, int> dists = new Dictionary<int, int>();
				int d = Mathf.RoundToInt((a.LocalPosition - b.LocalPosition).sqrMagnitude);
				dists.Add(pb.NodeIndex, d);
				_distances.Add(pa.NodeIndex, dists);
				return d;
			}
		}

		public override IEnumerable<Node> Neighbours(Node node)
		{
			List<Node> n = new List<Node>();
			foreach (int neighbour in (node as PathNode).Neighbours)
				n.Add(GetNodeFromIndex(neighbour));
			return n;
		}

		public override Node WorldPositionToNode(Vector3 worldPosition)
		{
			PathNode node = null;
			float dist = float.MaxValue;
			Vector3 localPos = worldPosition - _worldPosition;
			for (int i = 0; i < _nodes.Count; i++)
			{
				float d = (localPos - _nodes[i].LocalPosition).sqrMagnitude;
				if (d < dist)
				{
					node = _nodes[i];
					dist = d;
				}
			}
			return node;
		}

        public override Node LocalPositionToNode(Vector3 localPosition)
        {
            PathNode node = null;
            float dist = float.MaxValue;
            for (int i = 0; i < _nodes.Count; i++)
            {
                float d = (localPosition - _nodes[i].LocalPosition).sqrMagnitude;
                if (d < dist)
                {
                    node = _nodes[i];
                    dist = d;
                }
            }
            return node;
        }

        public override Node GetNodeFromIndex(int nodeIndex)
        {
            if (nodeIndex < 0 || nodeIndex >= _nodes.Count)
                return null;
            return _nodes[nodeIndex];
        }
    }
}
