﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
	public Waypoint[] Neighbors;
	public int Cost = 0;
	//public WaypointAndCost[] NeighboursAndCost;

	public bool Debug = true;
	public Color SelectedWayPoint = Color.green;
	public Color UnselectedWayPoint = new Color(0, 0, 1, 0.5f);
	public Color SelectedWayPointLine = Color.magenta;
	public Color UnselectedWayPointLine = new Color(1, 0, 0, 0.5f);

	public void OnDrawGizmos()
	{
		if (!Debug)
			return;
		Gizmos.color = UnselectedWayPoint;
		Gizmos.DrawSphere(transform.position, 0.2f);

		if (Neighbors != null)
		{
			Gizmos.color = UnselectedWayPointLine;
			for (int i = 0; i < Neighbors.Length; i++)
			{
				Gizmos.DrawLine(transform.position, Neighbors[i].transform.position);
			}
		}
	}

	public void OnDrawGizmosSelected()
	{
		if (!Debug)
			return;
		Gizmos.color = SelectedWayPoint;
		Gizmos.DrawSphere(transform.position, 0.2f);
		if (Neighbors != null)
		{
			Gizmos.color = SelectedWayPointLine;
			for (int i = 0; i < Neighbors.Length; i++)
			{
				Gizmos.DrawLine(transform.position, Neighbors[i].transform.position);
			}
		}
	}
}
