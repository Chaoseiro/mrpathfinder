﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
    [System.Serializable]
	public class GridMap : WeightedGraphMap
	{
        [SerializeField]
		int gridSizeX = 1;
        [SerializeField]
        int gridSizeY = 1;

		public int Width { get { return gridSizeX; } }
		public int Height { get { return gridSizeY; } }

		float _gridResolution = 1;
		public float GridResolution { get { return _gridResolution; } }

		Vector3 _worldTopLeft = Vector3.zero;

        [SerializeField]
		GridNode[] _nodes;

		public override Node[] Nodes
		{
			get
			{
				return _nodes;
			}
		}

        protected int GetIndex(int x, int y)
        {
            return y * gridSizeX + x;
        }

		public override int NodeCount
		{
			get { return gridSizeX * gridSizeY; }
		}

		public override Vector3 WorldPosition
		{
			get
			{
				return _worldPosition;
			}
			set
			{
				_worldPosition = value;
				switch (_axis)
				{
					case GraphAxis.XYZ:
					case GraphAxis.XandY:
						_worldTopLeft = _worldPosition - Vector3.right * _anchorPoint.x * gridSizeX * _gridResolution + Vector3.up * gridSizeY * _gridResolution * (1 - _anchorPoint.y);
						break;
					case GraphAxis.XandZ:
						_worldTopLeft = _worldPosition - Vector3.right * _anchorPoint.x * gridSizeX * _gridResolution + Vector3.forward * gridSizeY * _gridResolution * (1 - _anchorPoint.y);
						break;
					case GraphAxis.YandZ:
						_worldTopLeft = _worldPosition - Vector3.up * _anchorPoint.x * gridSizeX * _gridResolution + Vector3.forward * gridSizeY * _gridResolution * (1 - _anchorPoint.y);
						break;
				}
			}
		}

		public override int Heuristic(Node a, Node b)
		{
            // Manhattan
			GridNode ga = a as GridNode;
			GridNode gb = b as GridNode;
			int dstX = Mathf.Abs(ga.x - gb.x);
			int dstY = Mathf.Abs(ga.y - gb.y);

			if (dstX > dstY)
				return 14 * dstY + 10 * (dstX - dstY);
			return 14 * dstX + 10 * (dstY - dstX);
		}

		public override IEnumerable<Node> Neighbours(Node node)
		{
			List<Node> neighbours = new List<Node>();
			GridNode n = node as GridNode;

			// x - 1
			if (n.x - 1 >= 0)
				neighbours.Add(_nodes[GetIndex(n.x - 1, n.y)]);

			// x + 1
			if (n.x + 1 < gridSizeX)
				neighbours.Add(_nodes[GetIndex(n.x + 1, n.y)]);

			// y - 1
			if (n.y - 1 >= 0)
				neighbours.Add(_nodes[GetIndex(n.x, n.y - 1)]);

			// y + 1
			if (n.y + 1 < gridSizeY)
				neighbours.Add(_nodes[GetIndex(n.x, n.y + 1)]);

			// x - 1, y - 1
			if (n.x - 1 >= 0 && n.y - 1 >= 0)
				neighbours.Add(_nodes[GetIndex(n.x - 1, n.y - 1)]);

			// x + 1, y - 1
			if (n.x + 1 < gridSizeX && n.y - 1 >= 0)
				neighbours.Add(_nodes[GetIndex(n.x + 1, n.y - 1)]);

			// x - 1, y + 1
			if (n.x - 1 >= 0 && n.y + 1 < gridSizeY)
				neighbours.Add(_nodes[GetIndex(n.x - 1, n.y + 1)]);

			// x + 1, y + 1
			if (n.x + 1 < gridSizeX && n.y + 1 < gridSizeY)
				neighbours.Add(_nodes[GetIndex(n.x + 1, n.y + 1)]);

			return neighbours;
		}

		public override Vector3 WorldPositionToGraphPosition(Vector3 position)
		{
			Vector3 pos = Vector3.zero;

			switch (_axis)
			{
				case GraphAxis.XYZ:
				case GraphAxis.XandY:
					pos.x = (position.x - _worldTopLeft.x) / _gridResolution - _gridResolution / 2.0f;
					pos.y = (_worldTopLeft.y - position.y) / _gridResolution - _gridResolution / 2.0f;
					break;
				case GraphAxis.XandZ:
					pos.x = (position.x - _worldTopLeft.x) / _gridResolution - _gridResolution / 2.0f;
					pos.z = (_worldTopLeft.z - position.z) / _gridResolution - _gridResolution / 2.0f;
					break;
				case GraphAxis.YandZ:
					pos.y = (position.y - _worldTopLeft.y) / _gridResolution - _gridResolution / 2.0f;
					pos.z = (_worldTopLeft.z - position.z) / _gridResolution - _gridResolution / 2.0f;
					break;
			}

			return pos;//position - _worldTopLeft;
		}

        public Vector3 GridPositionToWorldPosition(int x, int y)
        {
            Vector3 pos = Vector3.zero;

            switch (_axis)
            {
                case GraphAxis.XYZ:
                case GraphAxis.XandY:
                    pos.x = _worldTopLeft.x + x * _gridResolution;
                    pos.y = _worldTopLeft.y - y * _gridResolution;
                    break;
                case GraphAxis.XandZ:
                    pos.x = _worldTopLeft.x + x * _gridResolution;
                    pos.z = _worldTopLeft.z - y * _gridResolution;
                    break;
                case GraphAxis.YandZ:
                    pos.y = _worldTopLeft.y + x * _gridResolution;
                    pos.z = _worldTopLeft.z - y * _gridResolution;
                    break;
            }

            return pos;
        }

        public Vector3 GridPositionToWorldPosition(Vector2 gridPos)
		{
			return GridPositionToWorldPosition(Mathf.RoundToInt(gridPos.x), Mathf.RoundToInt(gridPos.y));
		}

		public override Node WorldPositionToNode(Vector3 position)
		{
			int posX = 0;
			int posY = 0;
			Vector3 localPos = WorldPositionToGraphPosition(position);
			switch (_axis)
			{
				case GraphAxis.XYZ:
				case GraphAxis.XandY:
					posX = Mathf.RoundToInt(localPos.x);
					posY = Mathf.RoundToInt(localPos.y);
					break;
				case GraphAxis.XandZ:
					posX = Mathf.RoundToInt(localPos.x);
					posY = Mathf.RoundToInt(localPos.z);
					break;
				case GraphAxis.YandZ:
					posX = Mathf.RoundToInt(localPos.y);
					posY = Mathf.RoundToInt(localPos.z);
					break;
			}
			if (posX < 0)
				posX = 0;
			if (posX >= gridSizeX)
				posX = gridSizeX - 1;
			if (posY < 0)
				posY = 0;
			if (posY >= gridSizeY)
				posY = gridSizeY - 1;
			return _nodes[GetIndex(posX, posY)];
		}

        public override Node LocalPositionToNode(Vector3 localPosition)
        {
            int posX = 0;
            int posY = 0;
            switch (_axis)
            {
                case GraphAxis.XYZ:
                case GraphAxis.XandY:
                    posX = Mathf.RoundToInt(localPosition.x);
                    posY = Mathf.RoundToInt(localPosition.y);
                    break;
                case GraphAxis.XandZ:
                    posX = Mathf.RoundToInt(localPosition.x);
                    posY = Mathf.RoundToInt(localPosition.z);
                    break;
                case GraphAxis.YandZ:
                    posX = Mathf.RoundToInt(localPosition.y);
                    posY = Mathf.RoundToInt(localPosition.z);
                    break;
            }
            if (posX < 0)
                posX = 0;
            if (posX >= gridSizeX)
                posX = gridSizeX - 1;
            if (posY < 0)
                posY = 0;
            if (posY >= gridSizeY)
                posY = gridSizeY - 1;
            return _nodes[GetIndex(posX, posY)];
        }

        public GridMap(int gridWidth, int gridHeight, Vector3 gridPosition, Vector2 anchorPoint, float gridResolution = 1, GraphAxis axis = GraphAxis.XandY)
		{
			_gridResolution = gridResolution;
			if (_gridResolution <= 0)
				_gridResolution = 1;
			if (_gridResolution > gridWidth)
				_gridResolution = gridWidth;
			if (_gridResolution > gridHeight)
				_gridResolution = gridHeight;

			gridSizeX = Mathf.FloorToInt(gridWidth / _gridResolution);
			gridSizeY = Mathf.FloorToInt(gridHeight / _gridResolution);

			_nodes = new GridNode[gridSizeX * gridSizeY];

			_anchorPoint = anchorPoint;
			_axis = axis;
			WorldPosition = gridPosition;
		}

		public override void AddNode(Vector3 worldPosition, bool walkable, int cost)
		{
			int posX = 0;
			int posY = 0;
			Vector3 localPos = WorldPositionToGraphPosition(worldPosition);
			switch (_axis)
			{
				case GraphAxis.XYZ:
				case GraphAxis.XandY:
					posX = Mathf.FloorToInt(localPos.x);
					posY = Mathf.FloorToInt(localPos.y);
					break;
				case GraphAxis.XandZ:
					posX = Mathf.FloorToInt(localPos.x);
					posY = Mathf.FloorToInt(localPos.z);
					break;
				case GraphAxis.YandZ:
					posX = Mathf.FloorToInt(localPos.y);
					posY = Mathf.FloorToInt(localPos.z);
					break;
			}
			posY = Mathf.Abs(posY);
			if (posX < 0)
				posX = 0;
			if (posX >= gridSizeX)
				posX = gridSizeX - 1;
			if (posY < 0)
				posY = 0;
			if (posY >= gridSizeY)
				posY = gridSizeY - 1;
			_nodes[GetIndex(posX, posY)] = new GridNode(posX, posY, GetIndex(posX, posY), localPos, walkable, cost);

		}

		public override void AddNode(Node node)
		{
			GridNode n = node as GridNode;
			n.LocalPosition -= _worldPosition;
            if (n.x >= 0 && n.y >= 0 && n.x < gridSizeX && n.y < gridSizeY)
            {
                int index = GetIndex(n.x, n.y);
                _nodes[index] = n;
                _nodes[index].NodeIndex = index; // guarantee node index is correct
            }
		}

        public void AddNode(GridNode n, bool fixLocalPosition = true, bool fixIndex = true)
        {
            if(fixLocalPosition)
                n.LocalPosition -= _worldPosition;
            if (n.x >= 0 && n.y >= 0 && n.x < gridSizeX && n.y < gridSizeY)
            {
                int index = GetIndex(n.x, n.y);
                _nodes[index] = n;
                if(fixIndex)
                    _nodes[index].NodeIndex = index; // guarantee node index is correct
            }
        }

        public override Node GetNodeFromIndex(int nodeIndex)
        {
            if (nodeIndex >= 0 && nodeIndex < _nodes.Length)
            {
                return _nodes[nodeIndex];
            }

            return null;
        }

        public Node GetNodeFromIndex(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < gridSizeX && y < gridSizeY)
            {
                return _nodes[GetIndex(x, y)];
            }

            return null;
        }

        public override void RemoveNode(Node node)
        {
            GridNode n = node as GridNode;
            if (n.x >= 0 && n.y >= 0 && n.x < gridSizeX && n.y < gridSizeY)
            {
                _nodes[GetIndex(n.x, n.y)] = null;
            }
        }

        public override int Cost(Node a, Node b)
        {
            return a.Cost;
        }
    }
}
