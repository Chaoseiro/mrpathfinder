﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
	public interface IPathFindManager<T>
		//where L : Node
		where T : WeightedGraphMap
	{
		void RequestPath(Vector3 pathStart, Vector3 pathEnd, T graph, System.Action<Vector3[], bool> callback);
		void FinishedProcessingPath(Vector3[] path, bool success);
	}
}