﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
	public enum GraphAxis
	{
		XandY,
		XandZ,
		YandZ,
		XYZ
	}

	[System.Serializable]
	public class GraphCostPerLayer
	{
		public LayerMask Layer;
		public int Cost;
	}
    /// <summary>
    /// This is just a generic map base
    /// </summary>
    [System.Serializable]
    public abstract class WeightedGraphMap
	{
		protected Vector3 _worldPosition = Vector3.zero;
		protected Vector2 _anchorPoint = Vector2.zero;

		public virtual Vector3 WorldPosition
		{
			get { return _worldPosition; }
			set { _worldPosition = value; }
		}

		public virtual Vector2 AnchorPoint
		{
			get { return _anchorPoint; }
			set { _anchorPoint = value; }
		}

		protected GraphAxis _axis = GraphAxis.XandY;

		public virtual GraphAxis Axis
		{
			get { return _axis; }
			set { _axis = value; }
		}
        
		public abstract Node[] Nodes
		{
            get;
		}

        public abstract int NodeCount { get; }

        public abstract int Cost(Node a, Node b);

        public abstract int Heuristic(Node a, Node b);

		public virtual IEnumerable<Node> Neighbours(Node node)
		{
			return null;
		}

        public abstract Node WorldPositionToNode(Vector3 worldPosition);

        public abstract Node LocalPositionToNode(Vector3 localPosition);

        public abstract Node GetNodeFromIndex(int nodeIndex);

        public virtual Vector3 WorldPositionToGraphPosition(Vector3 worldPosition)
		{
			return Vector3.zero;
		}

		public virtual Vector3 NodePositionToWorldPosition(Node node)
		{
			return _worldPosition + node.LocalPosition;
		}

		public virtual Vector3 NodePositionToWorldPosition(Vector3 nodeLocalPosition)
		{
			return _worldPosition + nodeLocalPosition;
		}

        public abstract void AddNode(Vector3 worldPosition, bool walkable, int cost);

        public abstract void AddNode(Node node);

		public virtual void SetNodes(IEnumerable<Node> nodes)
		{
			throw new System.NotImplementedException();
		}

        public abstract void RemoveNode(Node node);
	}
}
