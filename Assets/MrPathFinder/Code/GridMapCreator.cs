﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
    [System.Serializable]
	public class GridMapCreator : MonoBehaviour
	{
		/// <summary>
		/// How many Units will the grid's Width be
		/// </summary>
		public int GridWidth = 10;
		/// <summary>
		/// How many Units will the grid's Height be
		/// </summary>
		public int GridHeight = 10;

        /// <summary>
		/// How many Units will the raycast to check for collisions start
		/// </summary>
		public int RaycastHeight = 100;

        /// <summary>
		/// Set to True to make the costs favor same-level nodes
		/// </summary>
		public bool HeightAffectsCost = true;

        /// <summary>
        /// GridResolution is the size, in Units, of the Grid's square
        /// </summary>
        [Range(0.1f, 10)]
		public float GridResolution = 1;

		public Vector3 AnchorPoint = new Vector3(0.5f, 0.5f, 0.5f);

		public LayerMask UnwalkableLayers;

        public bool ShowGizmos = true;

		public bool Use2DColliders = false;

		public GraphAxis Axis = GraphAxis.XandY;

		public GraphCostPerLayer[] CostPerLayer;
        protected LayerMask walkableLayers;
		protected Dictionary<int, int> walkableLayersCost = new Dictionary<int, int>();

        [Header("For Waypoint Map")]
        public bool GenerateWaypointMap = false;
        public int JumpHeight = 6;
        public int JumpWidth = 3;
        public LayerMask OneWayPlatformsLayers;

        [Header("Generate Grid On Start")]
        public bool GenerateOnStart = true;

        [SerializeField] [HideInInspector]
        protected GridMap m_gridMap = null;
        public GridMap GridMap
        {
            get { return m_gridMap; }
            protected set { m_gridMap = value; }
        }

        [SerializeField] [HideInInspector]
        protected WaypointMap m_waypointMap = null;
        public WaypointMap WaypointMap
        {
            get { return m_waypointMap; }
            protected set { m_waypointMap = value; }
        }


        // Use this for initialization
        void Start()
		{
            if(GenerateOnStart)
			    GenerateGrid();
		}
        [ContextMenu("Generate Grid")]
		public void GenerateGrid()
		{
            if (GridMap != null && GridMap.Width > 0)
                return;

			if (GridResolution > GridWidth)
				GridResolution = GridWidth;
			if (GridResolution > GridHeight)
				GridResolution = GridHeight;

			Vector2 halfRes = new Vector2(GridResolution / 2.0f, GridResolution / 2.0f);

			GridMap = new GridMap(GridWidth, GridHeight, transform.position, AnchorPoint, GridResolution, Axis);

			for (int i = 0; i < CostPerLayer.Length; i++)
			{
				walkableLayers.value |= CostPerLayer[i].Layer.value;
				walkableLayersCost.Add((int)Mathf.Log(CostPerLayer[i].Layer.value, 2), CostPerLayer[i].Cost);
			}

			for (int x = 0; x < GridMap.Width; x++)
			{
				for (int y = 0; y < GridMap.Height; y++)
				{
					Vector3 pos = Vector3.zero;
					pos = GridMap.GridPositionToWorldPosition(x, y);
					switch (Axis)
					{
						case GraphAxis.XandY:
							pos.x += halfRes.x;
							pos.y -= halfRes.y;
							break;
						case GraphAxis.XandZ:
							pos.x += halfRes.x;
							pos.z -= halfRes.y;
							break;
						case GraphAxis.YandZ:
							pos.y += halfRes.x;
							pos.z -= halfRes.y;
							break;
					}
					
					bool walkable = true;
					int cost = 0;
					if (Use2DColliders)
					{
						Collider2D coll = Physics2D.OverlapArea((Vector2)pos - halfRes * 0.9f, (Vector2)pos + halfRes * 0.9f, UnwalkableLayers);
						walkable = coll == null;

						if (walkable)
						{
							coll = Physics2D.OverlapArea((Vector2)pos - halfRes * 0.9f, (Vector2)pos + halfRes * 0.9f, walkableLayers);
							if (coll != null)
							{
								walkableLayersCost.TryGetValue(coll.gameObject.layer, out cost);
							}
						}
					}
					else
					{
						Vector3 direction = Vector3.zero;
						Vector3 origin = pos;
						switch (Axis)
						{
							case GraphAxis.XandY:
								direction = Vector3.forward;
								origin += Vector3.back * RaycastHeight * (1 - AnchorPoint.z);
								break;
							case GraphAxis.XandZ:
								direction = Vector3.down;
								origin += Vector3.up * RaycastHeight * (1 - AnchorPoint.z);
								break;
							case GraphAxis.YandZ:
								direction = Vector3.right;
								origin += Vector3.left * RaycastHeight * (1 - AnchorPoint.z);
								break;
						}
						RaycastHit hit;
						if (Physics.Raycast(origin, direction, out hit, RaycastHeight, walkableLayers))
						{
                            pos = hit.point;

                            walkable = !Physics.CheckSphere(pos, GridResolution * 0.49f, UnwalkableLayers);
                            if (walkable)
                            {
                                walkableLayersCost.TryGetValue(hit.collider.gameObject.layer, out cost);

                                if (HeightAffectsCost)
                                {
                                    switch (Axis)
                                    {
                                        case GraphAxis.XandY:
                                            cost += Mathf.FloorToInt(Mathf.Abs(pos.z));
                                            break;
                                        case GraphAxis.XandZ:
                                            cost += Mathf.FloorToInt(Mathf.Abs(pos.y));
                                            break;
                                        case GraphAxis.YandZ:
                                            cost += Mathf.FloorToInt(Mathf.Abs(pos.x));
                                            break;
                                    }
                                }
                            }
						}
					}
                    AddNode(x, y, pos, walkable, cost);
				}
			}
            if(GenerateWaypointMap)
                WaypointMap = GraphMapConverters.GridMapToWaypointMap(GridMap, JumpHeight, JumpWidth, UnwalkableLayers, OneWayPlatformsLayers);
        }

        protected virtual void AddNode(int x, int y, Vector3 pos, bool walkable, int cost)
        {
            GridMap.AddNode(new GridNode(x, y, x + y * GridWidth, pos, walkable, cost));
        }

        [ContextMenu("Clear Grid")]
        public void ClearGrid()
        {
            GridMap = null;
            if (walkableLayersCost != null)
                walkableLayersCost.Clear();
            walkableLayers.value = 0;

            WaypointMap = null;
        }

		public void OnDrawGizmos()
		{
			if (!ShowGizmos)
				return;
			Vector3 pos = transform.position;
			switch (Axis)
			{
				case GraphAxis.XandY:
					pos.x += GridWidth / 2.0f - AnchorPoint.x * GridWidth;
					pos.y += GridHeight / 2.0f - AnchorPoint.y * GridHeight;
                    pos.z += RaycastHeight / 2.0f - AnchorPoint.z * RaycastHeight;
                    Gizmos.DrawWireCube(pos, new Vector3(GridWidth, GridHeight, RaycastHeight));
					break;
				case GraphAxis.XandZ:
					pos.x += GridWidth / 2.0f - AnchorPoint.x * GridWidth;
					pos.z += GridHeight / 2.0f - AnchorPoint.y * GridHeight;
                    pos.y += RaycastHeight / 2.0f - AnchorPoint.z * RaycastHeight;
                    Gizmos.DrawWireCube(pos, new Vector3(GridWidth, RaycastHeight, GridHeight));
					break;
				case GraphAxis.YandZ:
					pos.y += GridWidth / 2.0f - AnchorPoint.x * GridWidth;
					pos.z += GridHeight / 2.0f - AnchorPoint.y * GridHeight;
                    pos.x += RaycastHeight / 2.0f - AnchorPoint.z * RaycastHeight;
                    Gizmos.DrawWireCube(pos, new Vector3(RaycastHeight, GridWidth, GridHeight));
					break;
			}
			
			if (GridMap != null && GridMap.Nodes != null)
			{
				for (int x = 0; x < GridMap.Width; x++)
				{
					for (int y = 0; y < GridMap.Height; y++)
					{
						var node = GridMap.GetNodeFromIndex(x, y);
                        if (node == null || node.NodeIndex < 0)
                            continue;

						Color c = node.IsWalkable ? Color.white : Color.red;
						c.a = (node.Cost + 1) / 10.0f;
						Gizmos.color = c;
						Gizmos.DrawWireCube(GridMap.NodePositionToWorldPosition(node), new Vector3(GridResolution * 0.9f, GridResolution * 0.9f, GridResolution * 0.9f));
					}
				}
			}

            if(WaypointMap != null && WaypointMap.Nodes != null)
            {
                foreach (var node in WaypointMap.Nodes)
                {
                    Gizmos.color = Color.black;
                    Gizmos.DrawSphere(node.LocalPosition, GridResolution * 0.2f);
                    foreach (var n in WaypointMap.Neighbours(node))
                    {
                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(node.LocalPosition, n.LocalPosition);
                    }
                }
                
            }
		}
	}
}