﻿using UnityEngine;
using System.Collections;
namespace LikeASir.MrPathfinder
{
	public class Seeker : MonoBehaviour
	{
		public Transform Target;
		public GridMapCreator GridMap;

		// Update is called once per frame
		void Update()
		{
			if (Input.GetButtonDown("Jump"))
			{
				targetIndex = 0;
				PathFindManager.Instance.RequestPath(transform.position, Target.position, GridMap.GridMap, OnPathFound);
			}
		}
		float speed = 20;
		Vector3[] path;
		int targetIndex;

		public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
		{
			if (pathSuccessful)
			{
				path = newPath;
				StopCoroutine("FollowPath");
				StartCoroutine("FollowPath");
			}
			else
				Debug.LogWarning("Path not found");
		}

		IEnumerator FollowPath()
		{
			if (path == null)
			{
				StopCoroutine("FollowPath");
				yield return null;
			}
			Vector3 currentWaypoint = path[0];

			while (true)
			{
				if ((transform.position - currentWaypoint).sqrMagnitude < 0.1f)
				{
					targetIndex++;
					if (targetIndex >= path.Length)
					{
						path = null;
						yield break;
					}
					currentWaypoint = path[targetIndex];
				}

				transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
				yield return null;

			}
		}

		void OnDrawGizmos()
		{
			if (path != null)
			{
				Gizmos.color = Color.cyan;
				for (int i = 0; i < path.Length - 1; i++)
				{
					Gizmos.DrawLine(path[i], path[i + 1]);
				}
			}
		}
	}
}