﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
    [System.Serializable]
    public class PathNode : Node
	{
        [SerializeField]
		protected List<int> _neighbours = new List<int>();
		public List<int> Neighbours { get { return _neighbours; } }

		public PathNode(int nodeIndex, Vector3 localPosition, bool walkable, int cost)
			: base(nodeIndex, localPosition, walkable, cost)
		{
		}

		public virtual void AddNeighbour(PathNode newNeighbour, bool oneWayNeighbour = false)
		{
			// check if neighbour is already added
			if (!_neighbours.Contains(newNeighbour.NodeIndex))
			{
				_neighbours.Add(newNeighbour.NodeIndex);
				// if it is not a one way connection, add this node to the new neighbour
				if (!oneWayNeighbour)
					newNeighbour.AddNeighbour(this);
			}
		}

		public virtual void RemoveNeighbour(PathNode neighbour)
		{
			_neighbours.Remove(neighbour.NodeIndex);
		}
	}
}