﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir.MrPathfinder
{
	public static class GraphMapConverters
	{
		public static WaypointMap GridMapToWaypointMap(GridMap gridMap, int jumpWidth, int jumpHeight, LayerMask unwalkableMask, LayerMask oneWayPlatforms)
		{
			WaypointMap waypointMap = new WaypointMap(gridMap.WorldPosition, gridMap.AnchorPoint, gridMap.Axis);
			List<PathNode> ways = new List<PathNode>();
            Node nodeA, nodeB;
            Vector3 posA, posB;
            RaycastHit2D[] linecastResults = new RaycastHit2D[5];
            int linecastCount = 0;
            // First, we add all grid nodes where an enemy can stand (nodes where y + 1 neighbour is not walkable or there is an one-way platform)
            for (int x = 0; x < gridMap.Width; x++)
			{
				for (int y = 0; y < gridMap.Height - 1; y++)
				{
                    nodeA = gridMap.GetNodeFromIndex(x, y);
                    if (nodeA != null && nodeA.IsWalkable)
					{
                        posA = gridMap.NodePositionToWorldPosition(nodeA);
                        posB = posA + Vector3.down;
                        linecastCount = Physics2D.LinecastNonAlloc(posA, posB, linecastResults, oneWayPlatforms);

                        nodeB = gridMap.GetNodeFromIndex(x, y + 1);
                        if (linecastCount > 0 || (nodeB != null && !nodeB.IsWalkable))
						{
							ways.Add(
								new PathNode(
                                    ways.Count,
									new Vector3(x + 0.5f, -y - 0.5f) * gridMap.GridResolution,
									true, 0));
						}
					}
				}
			}
            // Then we connect the adjacent nodes (same y value), as they are guaranteed to form a path.
            // Nodes that are more than jumpWidth units apart are not connected, since the gap is too large for jumps
            PathNode pathNodeA, pathNodeB;
            for (int i = 0; i < ways.Count - 1; i++)
			{
                pathNodeA = ways[i];
                posA = gridMap.NodePositionToWorldPosition(pathNodeA);
                for (int j = i + 1; j < ways.Count; j++)
				{
                    pathNodeB = ways[j];
                    posB = gridMap.NodePositionToWorldPosition(pathNodeB);

                    float difX = Mathf.Abs(pathNodeA.LocalPosition.x - pathNodeB.LocalPosition.x);
					float difY = Mathf.Abs(pathNodeA.LocalPosition.y - pathNodeB.LocalPosition.y);
					if (pathNodeA.LocalPosition.y == pathNodeB.LocalPosition.y)
					{
						// we just allow jumpWidth units
						if (difX <= jumpWidth)
						{
							// Linecast to check if there is a way between these nodes
							if (Physics2D.LinecastNonAlloc(posA, posB, linecastResults, unwalkableMask) < 1)
								pathNodeA.AddNeighbour(pathNodeB);
						}
					}

					// connect those where there is an adjacent node in the neighbourhood (difference in x < jumpWidth && difference in y < jumpHeight)
					if (difX <= jumpWidth && difY <= jumpHeight)
					{
                        if (difX < 2 * gridMap.GridResolution && difY < 2 * gridMap.GridResolution)
                        {
                            pathNodeA.AddNeighbour(pathNodeB);
                        }
                        else
                        {
                            // Linecast to check if there is a way between these nodes
                            if (Physics2D.LinecastNonAlloc(posA, posB, linecastResults, unwalkableMask) < 1)
                                pathNodeA.AddNeighbour(pathNodeB);
                        }
					}

					// make one-way connections from platforms to the ground (difference between x is just 1 but the difference between y > 2)
					if (Mathf.Abs(difX) <= gridMap.GridResolution && difY > jumpHeight)
					{
						// are we checking left or right?
						Vector3 checkDirection = new Vector2(pathNodeB.LocalPosition.x - pathNodeA.LocalPosition.x - 0.4f * gridMap.GridResolution, 0);
                        Vector3 startCheckDirection;
                        // check which node is higher, then check if there is no platform blocking this connection
                        if (pathNodeA.LocalPosition.y < pathNodeB.LocalPosition.y)
						{
							// B is higher
							checkDirection.y = gridMap.GridResolution;
							checkDirection.x += gridMap.GridResolution / 2.0f;

                            startCheckDirection = posA;

                            checkDirection = posB - checkDirection;

                            if (Physics2D.LinecastNonAlloc(startCheckDirection, checkDirection, linecastResults, unwalkableMask) < 1)
                            {
                                Node n = gridMap.WorldPositionToNode(checkDirection);
                                if (n.IsWalkable)
                                    pathNodeB.AddNeighbour(pathNodeA, true);
                            }
						}
						else
						{
							// A is higher
							checkDirection.y -= gridMap.GridResolution;

                            startCheckDirection = posB;

                            checkDirection += posA;

                            if (Physics2D.LinecastNonAlloc(startCheckDirection, checkDirection, linecastResults, unwalkableMask) < 1)
                            {
                                Node n = gridMap.WorldPositionToNode(checkDirection);
                                if (n.IsWalkable)
                                    pathNodeA.AddNeighbour(pathNodeB, true);
                            }
						}
					}
				}
			}

			// TODO: waypoint reduction
			//// Now we remove unnecessary nodes, where movement direction does not change,
			//// but we keep any node that has neighbours to a different direction
			//List<int> toRemove = new List<int>();
			//Vector2 directionOld = Vector2.zero;
			//for (int i = 1; i < ways.Count; i++)
			//{
			//	Vector2 directionNew = ways[i - 1].position - ways[i].position;
			//	// check the neighbours, if any neighbour has a different direction, this node stays
			//	if (directionNew == directionOld)
			//	{
			//		bool mustStay = false;
			//		foreach (var n in ways[i].Neighbours)
			//		{
			//			Vector2 d = n.position - ways[i].position;
			//			//if (Mathf.Abs(d.x - directionNew.x) > 0.1f || Mathf.Abs(d.y - directionNew.y) > 0.1f)
			//			if (Mathf.Sign(d.x) != Mathf.Sign(directionNew.x) || Mathf.Sign(d.y) != Mathf.Sign(directionNew.y))
			//			{
			//				mustStay = true;
			//				break;
			//			}
			//		}
			//		if (!mustStay)
			//		{
			//			// remove the node, but link the neighbours
			//			foreach (var n in ways[i].Neighbours)
			//				n.AddNeighbour(ways[i - 1]);
			//			toRemove.Add(i);
			//		}
			//	}
			//	directionOld = directionNew;
			//}
			//for (int i = 0; i < toRemove.Count; i++)
			//{
			//	ways.RemoveAt(toRemove[i]);
			//}

			waypointMap.SetNodes(ways);

			return waypointMap;
		}
	}
}
