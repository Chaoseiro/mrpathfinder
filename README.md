# MrPathfinder #

This plugin is a simple A* pathfinder for Unity3D 4.3+

It is compatible with [X-UniTMX](https://bitbucket.org/Chaoseiro/x-unitmx), but requires you to manually add it to the Assets folder (it will also need [CharacterController2D](https://github.com/prime31/CharacterController2D) if you want to test PathFindTMX scene)

It includes a simple Grid Map component which can generate a grid map with walkable/unwalkable cells and their costs, and you can also create through code Grid and Waypoint maps, as well as extend from WeightedGraphMap your own map representation to use with the A* algorithm.